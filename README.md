ics-ans-gitlab
==============

Ansible playbook to install GitLab.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
