import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('gitlab')


def test_gitlab_ce_installed(host):
    assert host.package('gitlab-ee').is_installed
